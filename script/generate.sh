#! /bin/bash

echo '---------- Create dist folder ----------'
mkdir dist

echo '---------- Generate pdf file from markdown ----------'
for md_file in $(ls src)
do
	pdf_file=$(echo $md_file | sed 's/.md/.pdf/')
	echo "generate $pdf_file." 
	md2pdf --css=assets/style.css src/$md_file dist/$pdf_file
	echo "$pdf_file successfuly created."
done

echo '---------- All pdf files have successfuly been created. ---------'
