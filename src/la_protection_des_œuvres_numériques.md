# La protection des œuvres numériques

## Le droit d’auteur à l’heure d’Internet

* La propriété littéraire et artistique protège les œuvres de l’esprit.
  
  * 🧑‍💻 Les œuvres littéraires, artistiques graphiques, plastiques, etc;
  
  * 🎨 Les œuvres numériques dont le logiciel.

* Les idées et concepts ne sont pas protégés.

* Le droit d’auteur s’acquiert sans formalités, mais en cas de litige, l’auteur doit être en mesure de prouver qu’il est bien l’auteur :
  
  * E-Soleau auprès de l’Inpi conservé pendant 5 ans;
  
  * Dépôts de l’œuvre cher un notaire/huissier auprès d’une société d’auteurs.

## La notion d’œuvre

* Les créations ne sont protégées que pour autant qu’elles sont originales.

* L’originalité : expression juridique de la créativité de l’auteur. Empreinte de sa personnalité ou l’effort intellectuel.

## Les prérogatives attachées au droit d’auteur

* Droit auteur confère à l’auteur un droit de propriété exclusif sur sa création :
  
  * droits moraux (perpétuels, imprescriptibles et inaliénables)
    
    * divulgation;
    
    * paternité;
    
    * respect de l’intégrité de l’œuvre;
    
    * droit de repentir ou de retrait.
  
  * droits patrimoniaux (appliquent 70 ans après décès de l’auteur), ils peuvent être cédés à un tiers.

## Les bénéficiaires du droit d’auteur

Titulaire de droit n’est pas forcément l’auteur c’est-à-dire la personne physique à l’origine de la création (ex: Guino et Renoir).

## Le cas des œuvres numériques

* Principales œuvres numériques :
  
  * Logiciel → ensemble d’instructions ayant pour but d’accomplir des fonctions par un SI ainsi que la documentation associée;
  
  * Base de données;
  
  * Œuvre multimédia (site internet, application mobile, jeu vidéo, …)

* Éléments protégés :
  
  * Code source;
  
  * Code binaire (code source compilé);
  
  * Matériel de conception préparatoire;

* En revanche, l’algorithme et les fonctionnalités ne sont pas protégés.

* Droit conférés à l’auteur :
  
  * Droits patrimoniaux → droit d’effectuer ou d’autoriser à titre gratuit ou onéreux :
    
    * la reproduction permanente ou provisoire de votre logiciel;
    
    * la traduction, l’adaptation ou la modification de votre logiciel.
  
  * Droits moraux → droits à la paternité mais les droits de retrait de de repentir disparaissent.

### Titularité des droits d’auteur sur le logiciel

* Principe : celui qui a créé le logiciel est titulaire des droits **MAIS** :
  
  * si le logiciel est œuvre collaboration entre divers auteurs : co-titulaires des droits;
  
  * si le logiciel a été crée à l’initiative d’une personne physique ou morale qui l’édite/publie/divulgue : c’est cette personne qui est considérée comme propriétaire;
  
  * si le logiciel est œuvre d’une commande, l’auteur reste titulaire sauf dans le cas où le contrat de licence ou de cession a été signé;
  
  * si le logiciel a été créé par un salarié qui opère dans le cadre de ses fonction : c’est ce dernier qui sera titulaire.

## L’action en contrefaçon

* L’acquisition du droit d’auteur permet d’agir devant la justice en contrefaçon
* Contrefaçon se définit comme la *reproduction, l’imitation ou l’utilisation totale ou partielle d’un droit de propriété intellectuelle sans l’autorisation de son propriétaire*.
* Action en contrefaçon :
  * au civil par le versement de dommages & intérêts;
  * au pénal par une amende de 300K€ et 3 ans d’emprisonnement.
* La preuve de la contrefaçon est libre et peut se faire par tous les moyens: 
  * Saisie-contrefaçon : procédure par laquelle un huissier de justice peut inspecter les locaux du contrefacteur.
  * Insertion de données-pièges : il peut s’agir d’erreurs dans un texte, de fauses entrées dans une BDD ou encore de code mort.
* HADOPI : promulguée en 2009, chargée de lutter contre le téléchargement illégal.
* Mutations dans le téléchargement illégal :
  * Pair-à-pair
  * streaming
  * direct

## Les licences d’exploitation

* L’autorisation d’utiliser des contenus protégés se formalise soit par la cession de droit soit par la concession et nécessite un certain formalisme

* Le contrat de licence d’utilisation du logiciel est le contrat par lequel les titulaires de droits sur le logiciel mettent leurs produits à la disposition de leurs clients.

* Il ne s’agit pas d’un transfert de droits mais simplement d’une concession d’un droit d’usage.

### Les différents types de licences

* 🔒 Licence propriétaire ou privative :
  
  * Titulaire des droits se réserve l’intégralité des droits;
  
  * Licencié a uniquement un droit d’usage;
  
  * Licence peut faire l’objet d’un paiement ou être à titre gratuit.

* 🕊️ Licence libre :
  
  * Donne l’autorisation gratuite à tous et par avance d’utiliser son œuvre dans les conditions fixées dans la licence.

### Le cas des licences créatives commons

* Créées en 2001 aux USA.

* Ont pour but d’encourage la circulation des œuvres sur Internet.

# 
