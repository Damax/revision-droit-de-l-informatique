# L’encadrement des données à caractères personnel

## L’internet et l’économie fondée sur la donnée : Big data

* Quoi ? Mégadonnées ou Données massives => enregistrer un très grand volume de données.

* Quand ? Depuis la démocratisation d’internet (2000)

* Comment ? Déploiement de « cloud computing » et des technologies de traitement performantes.

* Quelles caractéristiques ? Volume, vélocité et variété, valeur, véracité

## Objectifs et dérives

* Objectifs : 
  
  * Mieux connaître l’internaute
  
  * Aider à la prise de décision jusqu’à prédire les comportements
  
  * Mieux contrôler

* Opportunités et dérives
  
  * Amélioration des conditions de vie, de la sécurité, prise de décision, personnalisation des services…
  
  * Surveillance généralisé des individus (cf. Chine)
  
  * Problème de sécurité
  
  * Risques éthiques, sociaux, écologiques, politiques…

## Encadrement par le Droit

* 1974  : Affaire Safari

* 6 janvier 1978 : Création de la CNIL.
  
  * La finalité doit être déterminée, légitime et explicite.
  
  * La finalité permet de déterminer la pertinence des données personnelles recueillis.
  
  * La finalité doit être respectée.
  
  * La finalité permet de fixer la durée de conservation des données.

* 8 octobre 2016 : Loi pour une République Numérique : favorise l’ouverture et la circulation des données.

* 2018 : Création du RGPD :
  
  * DCP possible s’il se fonde sur une base légale :
    
    * Consentement
    
    * Contrat
    
    * Obligation légale
    
    * Mission d’intérêt public
    
    * Intérêt légitime
    
    * Sauvegarde des intérêts vitaux
  
  * Responsable du traitement dois :
    
    * Définir la finalité pour chaque traitement
    
    * Réaliser une analyse d’impact
    
    * Prendre toutes les mesures en matière de sécurisation des données
    
    * Fixer une durée de conservation de ces données
    
    * Faire respecter les droits des personnes en les informant
  
  * « Privacy by design and by Default » : Protection des données dès la conception du service/produit

* La CNIL peut désormais prononcer des sanctions pécuniaires jusqu’à 20 Millions d’euros ou 4% du chiffre d’affaires annuel mondial.
