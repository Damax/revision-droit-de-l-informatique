# La cybercriminalité

Regroupe infractions pénales à l’encontre d’un système d’informations.

## Les aspects de la cybercriminalité

* De l’individu isolé …
  * usurpateurs d’identité;
  * haters;
  * pédophiles; 
  * extrémistes
* … à des organisations parfois étatiques
  * hacktivistes (Wikileaks 💙);
  * cybercriminels;
  * états concurrents et la cyberguerre (sabotage, intimidation).

## Le rôle du darknet

Réseau dans lequel circulent des données qui ne sont pas référencées par les moteurs de recherche classiques. Le partage de données y est rendu anonyme par l’utilisateur de navigateur spécifique (Tor 💜).

## Les organismes de lutte contre la cybercriminalité

* ANSSI : Agence Nationale de la Sécurité des Systèmes d’Information;

* BEFTI : Brigade d’Enquêtes sur les Fraudes aux Technologies de l’Information;

* PHAROS : Plateforme d’Harmonisation d’Analyse, de Regroupement et d’Orientation des Signalements.

## Le cadre législatif

* Loi Godfrain du 6 janvier 1988 réprime les atteintes aux systèmes de traitements automatisés de données, atteintes sanctionnées par le code pénal comme délits :
  
  * Accéder ou se maintenir frauduleusement dans un système de traitement;
  
  * Introduire frauduleusement des données;
  
  * Extraire, détenir, reproduire, transmettre, supprimer ou modifier frauduleusement des données
  
  * Entraver ou fausser le fonctionnement d’un système de traitement de données.
  
  * Importer, détenir, offrir, céder ou mettre à disposition un équipement/instrument/programme informatique ou toute donnée conçus ou spécialement adaptés pour commettre une infraction.

* Principe de réalité :
  
  * Difficile d’identifier les attaquant
  
  * Peu de condamnation pour atteinte au stad.
  
  * Obliger les acteurs à sécuriser leur SI.

* Entreprise qui ne sécurise pas son SI peut être sanctionné de manière pécuniaire.

## L’adaptation du droit

* Hacker éthique peut prendre contact avec ANSSI pour signaler une vulnérabilité.

* Grandes entreprises mettent en place des programmes de « bug bounty », rémunèrent les chercheurs qui découvrent et communiquent les détails des fails.

* 21 janvier 2021 : « B0unty Factory » fondé par « Yes we hack » (responsable de la sécurité de Qwant)

## Point RGPD

* Oblige la sécurité des traitements en mettant des mesures techniques et organisationnelles appropriées afin de garantir un niveau de sécurité adapté au risque.

* Chercheurs ne sont pas exposés aux sanctions prévues par le code pénal.

## Préconisations

* Formation des magistrats au droit du numérique;

* Création d’un tribunal spécialisé;

* Renforcement de la coopération entre services (internationaux);

* Recrutement d’agents spécialisés dans la cybersécurité;

* Déposer plainte systématiquement et rapidement;

* Ne pas payer les rançons;

* Investir matériellement et humainement dans la cybersécurité.
