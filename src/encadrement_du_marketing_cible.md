# L’encadrement du marketing cible

## Le développement du e-commerce

* 40 millions de Français achètent sur Internet.

* CNIL a publié livre blanc sur données lors d’un paiement pour donner confiance, encadrer les pratiques et anticiper les transformations à venir.

## Le modèle économique d’internet

* 😤 Si c’est gratuit c’est que vous êtes le produit : fourniture de services apparemment gratuits mais financés majoritairement par la publicité. 

* 🏪 Les market places : plateformes mettent en relation des acheteurs et des vendeurs sur Internet.

* 🩸 Dons  : fonctionne uniquement grâce aux dons (Wikipédia, Framasoft…).

* 💸 Freemium : gratuit d’abord puis payant pour des contenus/services supplémentaires. 

## La publicité sur Internet

* 😠 La publicité personnalisée « classique » 

* 🤬 La publicité contextuelle 

* 😈 La publicité comportementale

## Le cadre juridique autour du marketing cible

### Site web, cookies et autres traceurs

* Consentement est la seule base légale qui légitime le dépôts de traceurs à des fins publicitaires.

* Cookies sont considérés comme des données personnelles

* L’internaute doit être informé par l’apparition d’un bandeau ou d’un pop up :
  
  * Finalités précises
  
  * Possibilité de s’opposer
  
  * Simple poursuite de navigation ne peut plus être regardée comme expression du consentement
  
  * Opérateurs exploitent des traceurs doivent prouver qu’ils ont bien recueilli le consentement.

* Les sites peuvent conditionner leur accès à un abonnement payant.

* Dans tous les cas, l’internaute doit pouvoir refuser les cookies à tout moment.

### La prospection commerciale par courrier électronique

* Pas de message commercial sans accord préalable du destinataire (RGPD ne modifie pas ces règles car il s’agit de B2B ou B2C).

* Chaque message électronique doit :
  
  * Préciser l’identité de l’annonceur.
  
  * Proposer un moyen simple de s’opposer à la réception de nouvelles sollicitations.
  
  * Si le consommateur ne répond à aucune sollicitation 3 ans après le dernier contact, les informations doivent être supprimées.

## Les biais autour du marketing ciblé

* 🥰 *L’éthique by design* : s’interroger dès la conception sur la manière dont les différentes techniques du design sont utilisées dans la mise en scène des services.

* 🥷🏿 Le recours aux *dark patterns* : astuces pour influencer l’utilisateur dans ces choix. Elles peuvent rester conformes au RGPD mais posent des questions éthiques.
