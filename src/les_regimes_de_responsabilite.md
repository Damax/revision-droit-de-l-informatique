# Les régimes de responsabilité

## Civile

* La victime peut saisir le tribunal → responsable indemnise son préjudice (corporel, moral, matériel).
* Obtention de dommages-intérêts
* Victime doit fixer préjudices subis + fournir des preuves

### Les différents régimes

- Élément légal

- Élément matériel

- Élément moral ou intentionnel

## Pénal

* La responsabilité « extra-contractuelle »

* La responsabilité non intentionnelle

* La responsabilité contractuelle

* → Exonération (force majeur, fiat d’un tiers, fait de la victime)

### Les différentes peines

* Les contraventions

* Les délits

* Les crimes

## L’organisation de la magistrature

* Les magistrats du siège (juges)

* Les magistrats du parquet (procureurs)
