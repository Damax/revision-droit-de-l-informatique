# Introduction générale au droit

## Le droit

* Ensemble de règles organisant la vie en société

* Trois caractères :
  
  * générale et impersonnelle
  
  * finalité sociale
  
  * coercitive

## La justice dans l’histoire

* Fin du Moyen Âge à la Révolution française : Toute justice émane du roi

* 1789 à 1799 : Élaboration de la *DDHC*.

* 1804 à 1814 : L’ère napoléonienne

* 1958 à nos jours : Une justice républicaine

## Les sources du droit

* La constitution française (législatif, exécutif, judiciaire).

* La loi

## L’organisation des sources du droit

* Principe de hiérarchisation (norme doit respecter celles qui se trouvent au niveau supérieur à la sienne).

* Droit National :
  
  * Public (Pénal, Constitutionnel, Administratif, Fiscal, Pénal)
  
  * Privé (Civil, Commercial, Social)

* 2 systèmes juridiques :
  
  * Juges liés par les codes
  
  * Juges ont un rôle prépondérant

## Les symboles et principes de la justice française

* Séparation des pouvoirs

* Gratuité de la justice

* Collégialité

* Égalité

* Publicité

* Dualité des juridictions

* Double degré de juridiction

* Présomption d’innocence

* Proportionnalité de la peine

* Contradictoire

* Non rétroactivité de la loi

* Possibilité de recours

## L’organisation judiciaire

* Civil : Arbitrer un conflit

* Pénal : Punir des actions répréhensibles (contraventions/délits/crimes).


