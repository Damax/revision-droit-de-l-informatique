# La responsabilité sur le web

## Les régimes de responsabilités sur le web

* Deux catégories de responsabilité
  
  * La responsabilité générale
    
    * L’éditeur de site internet (responsable de tous les contenus)
    
    * Les plateformes (clarté, transparence et loyauté)
    
    * Le moteur de recherche (déréférence des sites ou contenus litigieux)
    
    * Les internautes (engagée pour toutes actions causant dommage à autrui ou déterminant une infraction pénale).
  
  * La responsabilité spécifique
    
    * L’hébergeur ou prestataire de stockage (responsable que si notifié)
    
    * Le fournisseur d’accès à internet (pas obligation de surveiller)
    
    * Le commerçant en ligne (obligations spécifiques)
    
    * L’abonné (obligation de vigilance à l’absence de téléchargement illicite sur sa connexion).

## Les « nouvelles » infractions sur le web

* Infractions « spécifiées » ont été adaptées à Internet.

* Infractions « spécifiques » ont été créées pour s’adapter à l’environnement d’Internet.




