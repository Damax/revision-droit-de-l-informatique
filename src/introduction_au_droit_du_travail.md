# Introduction au droit du travail

## Les différents statuts

* L’informaticien indépendant

* L’informaticien salarié

## Le droit du travail

* Ensemble de règles qui régissent les rapports des salariés et de leur employeur.

* Différentes sources :
  
  * Le droit international
  
  * Le droit européen communautaire
  
  * Les sources étatiques internes
  
  * Les sources professionnelles internes

## Le contrat de travail

* Contrat par lequel une personne s’engage à travailler pour le compte et sous la direction d’une autre personne, moyennant rémunération.
  
  * La prestation de travail
  
  * La paiement d’une rémunération
  
  * Un lien de subordination

* Différent type de contrat :
  
  * Contrat de travail à durée indéterminée
  
  * Contrat à durée déterminée
  
  * Contrat de travail temporaire
  
  * Contrat d’apprentissages

## Les obligations issues du contrat de travail

* Droits du salarié :
  
  * Recevoir la rémunération prévue au contrat
  
  * Bénéficier des avantages sociaux
  
  * Droit à la formation
  
  * Droit au respect de sa vue privée
  
  * Droit à la déconnexion
  
  * Droit au télétravail

* Obligations du salarié :
  
  * Exécuter le travail
  
  * Prendre soin du matériel confié
  
  * Respecter le règlement intérieur de l’entreprise

* Droits de l’employeur :
  
  * Pouvoir normatif (élaborer règlement intérieur)
  
  * Pouvoir disciplinaire
  
  * Direction

* Obligations de l’employeur
  
  * Fournir le travail convenu
  
  * Verser le salaire
  
  * Respecter la loi

## Les clauses spécifiques au contrat de travail

* Lors de l’engagement du salarié
  
  * Clause d’essai

* Pendant l’exécution du contrat
  
  * Clause de mobilité
  
  * Clause d’exclusivité
  
  * Clause de débit formation

* Lors de la rupture du contrat de travail
  
  * Clause de non-concurrence

## La rupture du contrat de travail

* La rupture conventionnelle
  
  * Consentement des parties
  
  * Procédure légale
  
  * Homologuée par Directeur Départemental du Travail

* La rupture à l’initiative du salarié : la démission

* La rupture à l’initiative de l’employeur : le licenciement

## Le recours au tribunal des prud’hommes

* Juridiction d’exception compétent pour gérer les conflits individuels de travail

* Le conseiller prud’hommes est d’abord un salarié ou un employeur.

## Les ordonnances macron

* Inverser la hiérarchie des normes

* Le recours plus facile au CDD

* La rupture conventionnelle collective
